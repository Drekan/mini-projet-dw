CREATE TABLE TicketFact(
	purchase_date integer,
	booking_number integer,
	flight_number integer,
	promotion_number integer,
	ticket_type integer,
	dutyfree_price integer,
	full_price integer,
	tax_amount integer
);

ALTER TABLE TicketFact ADD FOREIGN KEY(purchase_date) REFERENCES DateDimension(id);
ALTER TABLE TicketFact ADD FOREIGN KEY(booking_number) REFERENCES BookingDimension(id);
ALTER TABLE TicketFact ADD FOREIGN KEY(flight_number) REFERENCES FlightDimension(id);
ALTER TABLE TicketFact ADD FOREIGN KEY(promotion_number) REFERENCES PromotionDimension(id);
ALTER TABLE TicketFact ADD FOREIGN KEY(ticket_type) REFERENCES TicketTypeDimension(id);


CREATE VIEW OnBoardSales_Date_Dim AS
SELECT id,date_description,full_date_description,day_of_week,week_number,calendar_month,calendar_year,holiday_indicator,weekday_indicator FROM DateDimension;

CREATE TABLE OnBoardSalesFact(
	purchase_date integer,
	flight_number integer,
	product_number integer,
	payment_method integer,
	quantity integer,
	dutyfree_price integer,
	full_price integer,
	tax_amount integer,
	benefit integer
);



ALTER TABLE OnBoardSalesFact ADD FOREIGN KEY(purchase_date) REFERENCES DateDimension(id);
ALTER TABLE OnBoardSalesFact ADD FOREIGN KEY(flight_number) REFERENCES FlightDimension(id);
ALTER TABLE OnBoardSalesFact ADD FOREIGN KEY(product_number) REFERENCES ProductDimension(id);
ALTER TABLE OnBoardSalesFact ADD FOREIGN KEY(payment_method) REFERENCES PaymentMethodDimension(id);
