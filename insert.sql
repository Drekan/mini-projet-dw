INSERT INTO PaymentMethodDimension VALUES (1,'Especes',NULL);
INSERT INTO PaymentMethodDimension VALUES (5,'CB','Crédit Agricole');
INSERT INTO PaymentMethodDimension VALUES (6,'CB','Caisse d Epargne');
INSERT INTO PaymentMethodDimension VALUES (7,'CB','LCL');
INSERT INTO PaymentMethodDimension VALUES (8,'CB','Banque populaire');
INSERT INTO PaymentMethodDimension VALUES (9,'CB','BNP');
INSERT INTO PaymentMethodDimension VALUES (10,'CB','Société Générale');
INSERT INTO PaymentMethodDimension VALUES (11,'CB','Citibank');

INSERT INTO ProductDimension VALUES (1,'Champagne','Taittinger','A fancy champagne digne des plus grands','Alcool','yes',500);
INSERT INTO ProductDimension VALUES (2,'Brownie','Nestlé','200g Browine, 75% cacao, 485kc','Nourriture','no',200);
INSERT INTO ProductDimension VALUES (3,'Pain','Carrefour','Baguette de pain française, cuite avec amour','Nourriture','no',100);
INSERT INTO ProductDimension VALUES (4,'Glaces','Gelati','Glace Italienne fait maison à Naples','Sucrerie','no',150);
INSERT INTO ProductDimension VALUES (5,'Bonbons','Haribo','Des schtroumpfs de formes diverses et variées','Sucrerie','no',300);
INSERT INTO ProductDimension VALUES (6,'Pois chiches','Belle-France','Les meilleurs pois chiches de tout le continent','Nourriture','no',1500);
INSERT INTO ProductDimension VALUES (7,'Soda','Coca-Cola','Le soda de la marque qui a créé le père noël','Boisson','yes',400);
INSERT INTO ProductDimension VALUES (8,'Riz','Taureau ailé','Un riz basmati de qualité supérieure','Nourriture','no',800);
INSERT INTO ProductDimension VALUES (9,'Eau','Cristaline','De l eau de source provenant du Massif Central','Boisson','yes',1);
INSERT INTO ProductDimension VALUES (10,'Chewing gum','Bubblegum','Un paquet de chewing gum avec emballage en papier','Surcerie','no',150);


INSERT INTO Customer VALUES (1,'Durant','Durant James','08/04/2019','France',30740,'Paris','yes');
INSERT INTO Customer VALUES (2,'SMITH','SMITH Mary','11/10/1991','Spain',30640,'Malaga','yes');
INSERT INTO Customer VALUES (3,'JOHNSON','JOHNSON Patricia','11/10/2009','Norway',30600,'Oslo','no');
INSERT INTO Customer VALUES (4,'WILLIAMS','WILLIAMS Jennifer','08/24/2007','Sweden',34090,'Stockholm','no');
INSERT INTO Customer VALUES (5,'BROWN','BROWN William','08/24/1987','Russia',78000,'St Petersbourg','yes');
INSERT INTO Customer VALUES (6,'JONES','JONES Barbara','09/03/1983','China',444455,'Xiamen','yes');
INSERT INTO Customer VALUES (7,'MILLER','MILLER Joseph','08/24/1987','North Korea',484844,'Pyongyang','yes');
INSERT INTO Customer VALUES (8,'MILLER','MILLER Andrea','08/24/1907','South Korea',878514,'Seoul','yes');
INSERT INTO Customer VALUES (9,'GARCIA','GARCIA Thomas','08/03/1982','Spain',323474,'Valencia','yes');
INSERT INTO Customer VALUES (10,'HERNANDEZ','HERNANDEZ Margaret','04/18/1993','Australia',14648,'Sydney','yes');
INSERT INTO Customer VALUES (11,'Schwarzenegger','Schwarzenegger Arnold','04/18/1993','France',14648,'Montpellier','yes');


Insert into DateDimension values (1,'2019/10/01',00,00,'Oct 1,2019', 'Tue', 56, 'Oct', '2019', 'Non-holiday', 'Weekday');
Insert into DateDimension values (2,'2019/10/01',03,45,'Oct 1,2019', 'Tue', 56, 'Oct', '2019', 'Non-holiday', 'Weekday');
Insert into DateDimension values (3,'2019/10/01',21,30,'Oct 1,2019', 'Tue', 56, 'Oct', '2019', 'Non-holiday', 'Weekday');
Insert into DateDimension values (4,'2019/10/05',10,00,'Oct 5,2019', 'Sat', 56, 'Oct', '2019', 'Non-holiday', 'Weekend');
Insert into DateDimension values (5,'2019/10/09',12,15,'Oct 9,2019', 'Wed', 57, 'Oct', '2019', 'Non-holiday', 'Weekday');
Insert into DateDimension values (6,'2019/11/01',07,45,'Nov 1,2019', 'Fri', 60, 'Nov', '2019', 'Holiday', 'Weekday');
Insert into DateDimension values (7,'2019/11/02',10,00,'Nov 2,2019', 'Sat', 60, 'Nov', '2019', 'Non-holiday', 'Weekend');
Insert into DateDimension values (8,'2019/12/25',12,25,'Dec 25,2019', 'Wed', 75, 'Dec', '2019', 'Holiday', 'Weekday');


Insert into Airplane values (1,'Boeing 737-800','Boeing Commercial Airplanes','737-800','1997/02/09',189,26022,44);
Insert into Airplane values (2,'Boeing 737-700','Boeing Commercial Airplanes','737-700','1967/05/09',149,26022,44);


Insert into Airport values (1,'Heathrow Airport','London','UK', 3600, 20, 58000);
Insert into Airport values(2,'CDG Airport','Paris','France', 1860, 28, 98040);
Insert into Airport values(3,'Frankfurt am Main Airport','Frankfurt','Germany', 1036, 19, 68500);
Insert into Airport values(4,'Barcelona-El Prat Josep Tarradellas Airport','Barcelona','Spain', 61, 27, 50172);
Insert into Airport values(5,'Gatwick Airport','London','UK', 14, 57, 57067);
Insert into Airport values(6,'Leonardo da vinci-Fiumicino Airport','Rome','Italy', 42, 49, 20232);


Insert into BookingDimension values (1,2,1,'USD');
Insert into BookingDimension values(2,1,1,'USD');
Insert into BookingDimension values(3,3,3,'GBP');
Insert into BookingDimension values(4,4,4,'USD');
Insert into BookingDimension values(5,4,5,'EUR');
Insert into BookingDimension values(6,5,7,'EUR');
Insert into BookingDimension values(7,7,8,'EUR');
Insert into BookingDimension values(8,10,8,'GBP');
Insert into BookingDimension values(9,11,8,'GBP');

INSERT INTO FlightDimension VALUES (1,1,1,2,1,5);
INSERT INTO FlightDimension VALUES (2,1,2,5,3,6);
INSERT INTO FlightDimension VALUES (3,1,3,7,4,4);
INSERT INTO FlightDimension VALUES (4,1,3,1,6,1);
INSERT INTO FlightDimension VALUES (5,1,1,8,4,6);
INSERT INTO FlightDimension VALUES (6,1,7,1,6,2);
INSERT INTO FlightDimension VALUES (7,1,3,2,1,4);
INSERT INTO FlightDimension VALUES (8,2,4,4,4,5);
INSERT INTO FlightDimension VALUES (9,2,8,8,2,4);
INSERT INTO FlightDimension VALUES (10,1,4,7,4,2);

INSERT INTO PromotionDimension VALUES (1,'Solde','Une promotion exceptionnelle',15,1,2,'60s');
INSERT INTO PromotionDimension VALUES (2,'Mega Solde','Une promotion exceptionnelle',10,2,3,'70s');
INSERT INTO PromotionDimension VALUES (3,'Black Christmas','Une promotion exceptionnelle',5,3,4,'Millenials');
INSERT INTO PromotionDimension VALUES (4,'Black Friday','Une promotion exceptionnelle',20,4,5,'80s');
INSERT INTO PromotionDimension VALUES (5,'Black Solde','Une promotion exceptionnelle',99,5,5,'Kids');
INSERT INTO PromotionDimension VALUES (6,'Summer holiday','Une promotion exceptionnelle',10,6,7,'60s');
INSERT INTO PromotionDimension VALUES (7,'Winter holiday','Une promotion exceptionnelle',7,7,4,'90s');
INSERT INTO PromotionDimension VALUES (8,'Thanksgiving Offer','Une promotion exceptionnelle',8,8,7,'90s');
INSERT INTO PromotionDimension VALUES (9,'Fall holiday','Une promotion exceptionnelle',23,4,3,'75s');
INSERT INTO PromotionDimension VALUES (10,'Spring holiday','Une promotion exceptionnelle',12,3,4,'2000s');

INSERT INTO TicketTypeDimension VALUES(1,'standard','Classe standard',0);
INSERT INTO TicketTypeDimension VALUES(2,'Plus','Classe +',50);
INSERT INTO TicketTypeDimension VALUES(3,'Flexi Plus','Flexi ++',70);
INSERT INTO TicketTypeDimension VALUES(4,'Family','Des enfants, eloignez vous',100);

/*TicketFact(purchase_date, booking_number, flight_number, promotion_number, ticket_type, dutyfree_price, full_price, tax_amount)*/
Insert into TicketFact values (1,1,1,NULL,1,100,120,20);
Insert into TicketFact values (3,3,2,9,2,500,560,60);
Insert into TicketFact values (5,2,3,NULL,3,499,500,1);
Insert into TicketFact values (4,4,4,NULL,4,693,700,7);
Insert into TicketFact values (1,5,6,6,1,965,1000,35);
Insert into TicketFact values (2,6,9,5,2,96,100,4);
Insert into TicketFact values (2,7,9,NULL,3,85,90,5);
Insert into TicketFact values (8,8,8,NULL,4,900,999,99);
Insert into TicketFact values (1,9,6,NULL,1,965,1000,35);


INSERT INTO OnBoardSalesFact VALUES(1,1,1,1,1,1,100,20,8);
INSERT INTO OnBoardSalesFact VALUES(2,2,2,1,2,17,18,21,7);
INSERT INTO OnBoardSalesFact VALUES(3,3,3,1,1,12,20,23,6);
INSERT INTO OnBoardSalesFact VALUES(4,4,4,1,3,3,6,20,6);
INSERT INTO OnBoardSalesFact VALUES(5,5,5,5,5,4,15,19,7);
INSERT INTO OnBoardSalesFact VALUES(6,6,6,6,1,5,6,18,5);
INSERT INTO OnBoardSalesFact VALUES(6,7,7,7,2,6,7,17,7);
INSERT INTO OnBoardSalesFact VALUES(7,8,8,8,6,3,4,20,9);
INSERT INTO OnBoardSalesFact VALUES(7,9,9,9,4,10,11,5,6);
INSERT INTO OnBoardSalesFact VALUES(8,10,10,10,2,7,8,4,4);
