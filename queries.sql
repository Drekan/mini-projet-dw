/*1 Nombre de tickets achetés, classés par destination (pays -> ville)*/
SELECT a.country,a.city,count(*) 
FROM TicketFact t,FlightDimension f, Airport a
WHERE t.flight_number = f.id and f.destination_airport = a.id
GROUP BY a.country,a.city
ORDER BY count(*) DESC;

/*2 Classement des vols par revenu décroissant*/
SELECT Depart.city AS Depart, Arrivee.city AS Arrivee, sum(full_price) AS Revenu, count(*) AS VOLS
FROM TicketFact t, FlightDimension f, Airport Depart, Airport Arrivee
WHERE t.flight_number = f.id AND
	f.starting_airport = Depart.id AND
	f.destination_airport = Arrivee.id
GROUP BY (Depart.city, Arrivee.city)
ORDER BY sum(full_price) DESC;

/*3 Revenus par Année et par mois*/
SELECT d.calendar_year AS Annee, d.calendar_month AS Mois, sum(full_price) AS Revenu
FROM TicketFact t, DateDimension d 
WHERE t.purchase_date = d.id
GROUP by ROLLUP(d.calendar_year,d.calendar_month)
ORDER BY sum(full_price) DESC;

/*4 âge moyen des clients par destination*/
SELECT Airport.country, Airport.city, AVG(2019-SUBSTR(Customer.date_of_birth,7,10)) AS Age_Moyen 
FROM Customer,BookingDimension,FlightDimension,Airport,TicketFact
WHERE TicketFact.flight_number = FlightDimension.id AND FlightDimension.destination_airport = Airport.id
AND BookingDimension.id = TicketFact.booking_number AND BookingDimension.customer_id = Customer.id
GROUP BY Airport.country, Airport.city
ORDER BY Age_Moyen Asc;

/*5 Nombre de ventes par type de ticket*/
SELECT t.ticket_type, s.name, count(t.ticket_type)
FROM TicketFact t, TicketTypeDimension s
where t.ticket_type=s.id
group by  t.ticket_type, s.name
order by count(t.ticket_type) desc;


/*6 Revenu total par vol (ville depart, ville arrivee) par an*/
SELECT  Depart.name AS Depart, Arrivee.name AS Arrivee,DateDimension.calendar_year AS ANNEE,sum(OnBoardSalesFact.full_price) AS Revenu
FROM OnBoardSalesFact, FlightDimension f, Airport Depart, Airport Arrivee, DateDimension
WHERE 
OnBoardSalesFact.flight_number = f.id AND 
OnBoardSalesFact.purchase_date = DateDimension.id AND
f.starting_airport = Depart.id AND f.destination_airport = Arrivee.id
GROUP BY   Depart.name, Arrivee.name, DateDimension.calendar_year
ORDER BY Revenu Desc;

/*7 Produit alimentaires les plus vendu par an*/
SELECT d.calendar_year, p.name, count(*) AS Vente
FROM OnBoardSalesFact o, ProductDimension p, DateDimension d
WHERE p.id = o.product_number and o.purchase_date = d.id AND (p.category = 'Nourriture' OR p.category = 'Boisson')
GROUP BY (p.name, d.calendar_year)
ORDER BY count(*);


/*8 Revenu total de on board sales par an */
SELECT d.calendar_year as ANNEE, sum(o.full_price) as Revenu
FROM OnBoardSalesFact o, DateDimension d
WHERE o.purchase_date=d.id
GROUP BY d.calendar_year
ORDER BY d.calendar_year;


/*9 Produits par rapport rentabilité/poids*/
SELECT Product.name, (OnBoard.full_price/OnBoard.quantity)/Product.weight AS Price_Per_G
FROM OnBoardSalesFact OnBoard, ProductDimension Product
WHERE OnBoard.product_number = Product.id
ORDER BY Price_Per_G Desc;

/*10 Revenus par type par ans*/
SELECT d.calendar_year AS Annee, p.type as typeDePayment, sum(o.full_price) as Revenu
FROM OnBoardSalesFact o, PaymentMethodDimension p, DateDimension d
where o.payment_method=p.id AND d.id = o.purchase_date
GROUP BY (p.type, d.calendar_year)
ORDER BY sum(o.full_price) DESC;


