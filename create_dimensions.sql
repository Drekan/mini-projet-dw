CREATE TABLE PaymentMethodDimension(
	id integer primary key,
	type varchar(50),
	bank varchar(50)
);


CREATE TABLE ProductDimension(
	id integer primary key,
	name varchar(50),
	brand varchar(50),
	description varchar(50),
	category varchar(50),
	liquid_indicator varchar(10),
	weight integer
);


CREATE TABLE Customer(
	id integer primary key,
	last_name varchar(50),
	full_name varchar(50),
	date_of_birth varchar(50),
	country varchar(20),
	zip_code integer,
	city varchar(50),
	majority_indicator varchar(50)
);


CREATE TABLE TicketTypeDimension(
	id integer primary key,
	name varchar(50),
	description varchar(50),
	refundRate integer
);

CREATE TABLE DateDimension(
	id integer primary key,
	date_description varchar(50),
	hour integer,
	minute integer,
	full_date_description varchar(50),
	day_of_week varchar(50),
	week_number integer,
	calendar_month varchar(10),
	calendar_year varchar(10),
	holiday_indicator varchar(50),
	weekday_indicator varchar(50)
);

CREATE TABLE Airplane(
	id integer primary key,
	name varchar(50),
	manufacturer varchar(50),
	plane_type varchar(50),
	construction_date varchar(50),
	number_of_seat integer,
	gastank_volume integer,
	hold_volume integer
);

CREATE TABLE Airport(
	id integer primary key,
	name varchar(50),
	city varchar(50),
	country varchar(50),
	capacity integer,
	landing_runway_number integer,
	employee_number integer
);

CREATE TABLE BookingDimension(
	id integer primary key,
	customer_id integer,
	date_id integer,
	currency varchar(20)
);

ALTER TABLE BookingDimension ADD FOREIGN KEY(customer_id) REFERENCES Customer(id);
ALTER TABLE BookingDimension ADD FOREIGN KEY(date_id) REFERENCES DateDimension(id);


CREATE TABLE PromotionDimension(
	id integer primary key,
	name varchar(50),
	description varchar(50),
	price_reduction varchar(50),
	begin_date integer,
	end_date integer,
	client_type_targeted varchar(50)
);

ALTER TABLE PromotionDimension ADD FOREIGN KEY(begin_date) REFERENCES DateDimension(id);
ALTER TABLE PromotionDimension ADD FOREIGN KEY(end_date) REFERENCES DateDimension(id);



CREATE TABLE FlightDimension(
	id integer primary key,
	airplane integer,
	starting_date integer,
	arrival_date integer,
	starting_airport integer,
	destination_airport integer
);

ALTER TABLE FlightDimension ADD FOREIGN KEY(starting_date) REFERENCES DateDimension(id);
ALTER TABLE FlightDimension ADD FOREIGN KEY(arrival_date) REFERENCES DateDimension(id);
ALTER TABLE FlightDimension ADD FOREIGN KEY(starting_airport) REFERENCES Airport(id);
ALTER TABLE FlightDimension ADD FOREIGN KEY(destination_airport) REFERENCES Airport(id);
